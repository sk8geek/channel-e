<!-- Copyright 1994-2010 Steven J Lilley -->
<head>
<title>channel-e</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="msnbot" content="noindex,nofollow">
<meta name="robots" content="index,follow">
<meta name="description" content="channel-e">
<meta name="keywords"    content="channel-e, information in an electronic age, comms, Psion, 3a, 3c, 3mx, software, authoring, html, web, site, website, www, design, coding, app, apps, application, applications, extract, questionnaire, people counter, APC, footpath, condition survey, program, programmer, bespoke, Java, servlet, JSP, applet, php, xhtml, Linux, Fedora, open, FLOSS, GPL, open source">
<link rel="stylesheet" type="text/css" href="channel-e.css">
<link rel="shortcut icon" href="images/channel-e.ico">
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7812648-10']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</head>
