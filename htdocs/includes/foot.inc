<div id="footer">
<p class="footer">CC 1994-2010 Steven J Lilley</p>

<p class="buttons">
<a href="http://validator.w3.org/check?uri=referer"><img src="images/valid-html401.png" alt="Valid HTML 4.01!" style="border:0;width:88px;height:31px;"></a>
          
<a href="http://jigsaw.w3.org/css-validator/validator?uri=http://www.channel-e.co.uk/channel-e.css">
<img style="border:0;width:88px;height:31px;" src="images/vcss.gif" alt="Valid CSS! "></a>

<!-- Creative Commons License -->
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/2.5/">
<img alt="Creative Commons License" style="border:0;width:88px;height:31px;" src="images/somerights20.gif"></a>
<!-- /Creative Commons License -->
<!--
<rdf:RDF xmlns="http://web.resource.org/cc/"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<Work rdf:about="">
   <dc:type rdf:resource="http://purl.org/dc/dcmitype/Text" />
   <license rdf:resource="http://creativecommons.org/licenses/by-nc-sa/2.5/" />
</Work>
<License rdf:about="http://creativecommons.org/licenses/by-nc-sa/2.5/">
   <permits rdf:resource="http://web.resource.org/cc/Reproduction" />
   <permits rdf:resource="http://web.resource.org/cc/Distribution" />
   <requires rdf:resource="http://web.resource.org/cc/Notice" />
   <requires rdf:resource="http://web.resource.org/cc/Attribution" />
   <prohibits rdf:resource="http://web.resource.org/cc/CommercialUse" />
   <permits rdf:resource="http://web.resource.org/cc/DerivativeWorks" />
   <requires rdf:resource="http://web.resource.org/cc/ShareAlike" />
</License>
</rdf:RDF>
-->

<a href="http://www.spreadfirefox.com/?q=affiliates&amp;id=13541&amp;t=68"><img alt="Get Firefox!" title="Get Firefox!" style="border:0;width:88px;height:31px;" src="images/firefox.png"></a>

<a href="http://www.openoffice.org"><img src="images/88x31_3.png" style="border:0;width:88px;height:31px;" alt=" Use OpenOffice.org" title="Use OpenOffice.org"></a>

<a href="http://www.redhat.com/"><img style="border:0;width:88px;height:31px;" src="images/poweredby.png" alt="Powered by RedHat"></a>
</p>
</div>
