<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!-- Copyright 1994-2004 Steven J Lilley -->
<head>
<title>channel-e, Reporter</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="index,follow" />
<meta name="description" content="Description of Reporter and DataReader applications.  Data collection tools for street wardens." />
<meta name="keywords"    content="Java, reporter, data collection, reader, Zaurus, handheld, PDA, MySQL, wireless" />
<link rel="stylesheet" type="text/css" href="channel-e.css" />
<link rel="shortcut icon" href="channel-e.ico" />
</head>
<body>

<div class="menu">
<jsp:useBean id="menu" scope="page" class="uk.co.channele.www.MenuBean">
<jsp:setProperty name="menu" property="pageID" value="java_reporter" />
<jsp:getProperty name="menu" property="pageMenu" />
</jsp:useBean> 
</div>

<p class="site_id">channel-e<br />
<span class="h1tag">information in an electronic age.</span></p>

<h1>Reporter and Data Reader Applications</h1>

<h2><a class="fragid" name="background">Background</a></h2>
<p>Our street wardens (at Calderdale) needed a data collection application for use on their handhelds.  I didn't have any time for development at work so I wrote these in my own time.  The wardens are using Sharp Zaurus machines.</p>

<h2><a class="fragid" name="operation">Operation</a></h2>
<p>These applications both run under a <abbr title="Graphical User Environment">GUI</abbr>.  Communication with the database is via wireless network.</p>

<h3>Reporter</h3>
<p>This was the first application written.  It allows the wardens to fill a form on the display.  Drop down lists are provided for warden name and action code.  These lists can be updated from the database.  On returned to the office the reports are added to the database.</p>

<h3>Data Reader</h3>
<p>The wardens wanted to be able to search existing reports whilst out in the field.  This application enables this by downloading a serialized copy of the reports table.  The file is recreated every four hours by the house keeping routine in the <a href="java_itres">itres</a> application suite.</p>


<h2><a class="fragid" name="status">Status</a></h2>
<p>No current activity in this package.  The <a href="java_itres">itres</a> suite is being developed to include:</p>

<dl>
<dt>Automated messaging.</dt>  
<dd>The report table will be checked for certain record types, such as abondoned vehicles.  Automated e-mails will be sent to other sections to initiate action.</dd>
</dl>

<p>I want to improve the record search speed of DataReader but I don't know how.</p>

<p>Released under the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License</a>.  <a href="ftp://www.channel-e.co.uk/pub/apps/java/reporter">download</a></p>

<h2><a class="fragid" name="history">History</a></h2>
<h3>Reporter</h3>
<p>At version 2.3 and now quite stable.</p> 

<h3>Data Reader</h3>
<p>At version 0.4.2.  Previous versions received the report file in plain text.  This version uses serialized ReportRecord objects.</p>

<p class="footer">copyright &copy;1994-2004 Steven J Lilley</p>


</body>
</html>