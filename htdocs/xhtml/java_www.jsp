<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!-- Copyright 1994-2004 Steven J Lilley -->
<head>
<title>channel-e, Misc. Servlets</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="index,follow" />
<meta name="description" content="Description of various servlets." />
<meta name="keywords"    content="Java, servlet, printer friendly" />
<link rel="stylesheet" type="text/css" href="channel-e.css" />
<link rel="shortcut icon" href="channel-e.ico" />
</head>
<body>

<div class="menu">
<jsp:useBean id="menu" scope="page" class="uk.co.channele.www.MenuBean">
<jsp:setProperty name="menu" property="pageID" value="java_parser" />
<jsp:getProperty name="menu" property="pageMenu" />
</jsp:useBean> 
</div>

<p class="site_id">channel-e<br />
<span class="h1tag">information in an electronic age.</span></p>

<h1>Miscellaneous Servlets</h1>

<h2><a class="fragid" name="background">Background</a></h2>
<h3>PrintPage</h3>
<p>The PrintPage servlet was one of my first.  We needed something to convert the complex pages on Calderdale's web site to printer friendly format.  From conversations I understood that they were having problems implementing this using <abbr title="Java Server Pages">JSP</abbr>s so I wrote this servlet.</p>

<h2><a class="fragid" name="operation">Operation</a></h2>
<p>Strips (then) Calderdale web pages of navigation.  Decodes image source references so that they are properly included.</p>

<h2><a class="fragid" name="status">Status</a></h2>
<p>No further development planned.</p>

<p>Released under the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License</a>.  <a href="ftp://www.channel-e.co.uk/pub/apps/java/www">download</a></p>

<h2><a class="fragid" name="history">History</a></h2>

<h3>PrintPage</h3>

<p>Version 2.0.1, 6 September 2004. Tidied up the code a little, moved variable to the beginning of the class.</p>

<p>I was bold with the version numbers in those days.  Version 2.0 dates from 6 July 2002.</p>

<p class="footer">copyright &copy;1994-2004 Steven J Lilley</p>


</body>
</html>