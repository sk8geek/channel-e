<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!-- Copyright 1994-2004 Steven J Lilley -->
<head>
<title>channel-e, GW Check Parser</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="index,follow" />
<meta name="description" content="Description of Groupwise check log file parser.  A Java application that summarises gwchk32 produced log files." />
<meta name="keywords"    content="Java, GroupWise, check, gwchk32, parse, log" />
<link rel="stylesheet" type="text/css" href="channel-e.css" />
<link rel="shortcut icon" href="channel-e.ico" />
</head>
<body>

<div class="menu">
<jsp:useBean id="menu" scope="page" class="uk.co.channele.www.MenuBean">
<jsp:setProperty name="menu" property="pageID" value="java_gwc" />
<jsp:getProperty name="menu" property="pageMenu" />
</jsp:useBean> 
</div>

<p class="site_id">channel-e<br />
<span class="h1tag">information in an electronic age.</span></p>

<h1>Groupwise Check Log File Parser</h1>

<h2><a class="fragid" name="background">Background</a></h2>
<p>We user Groupwise at work and we also use the check program, gwchk32.  This is a utility that will produce statistics, reduce mailbox sizes, purge large messages etc.  The functions that I find particularly useful are checking what proxy rights have been granted and checking for large messages (attachments actually).</p>

<h3>Checking Granted Proxy Rights</h3>
<p>In order to check the proxy rights that my (350) users have granted I need to run a verbose check.  This results in a log file of about 26Mb.  Searching a file this size for the lines detailing proxy rights is time consuming.  Not only that the rights granted are given in an encoded form.</p>

<h3>Checking for Large Messages</h3>
<p>Our post office directory is approximately 20Gb.  We've had Groupwise since April 1998.  At first I attributed the size of the post office to old messages.  In fact large messages take up a greater proportion of space.  I regulary check for large messages and ask users to deal with them.  (We have had users in the same office, who log in to the same server, mail large (100Mb+) attachments to each other when they are joint working!)</p>

<h2><a class="fragid" name="operation">Operation</a></h2>
<p>This is a very simple program that runs under a <abbr title="Graphical User Environment">GUI</abbr>.  The interface is split into three sections.  The first allows selection of an input log file.  The second presents proxy and sort options (sort options not active yet).  The last alows the results to be written to a file or to the display.  The program generates a tree and uses JTree to display it.</p>

<h2><a class="fragid" name="status">Status</a></h2>
<p>Still being actively developed, currently working on:</p>

<dl>
<dt>Implement message matching.</dt>  
<dd>The check program reports on all mailboxes.  Consequently, messages between users are included in the totals multiple times.  This makes the total artificially high.</dd>

<dt>Sorting of the results.</dt>
<dd>Currently the accounts are displayed in the order that gwchk32 lists them.  It would be useful to show them as largest total attachments first.</dd>

<dt>Automatic mailing of advisories.</dt>
<dd>I regularly check proxies granted and large message totals.  I then mail users and ask them to fix the problems.  It would be handy to mail directly from the application.</dd>
</dl>

<p>Released under the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License</a>.  <a href="ftp://www.channel-e.co.uk/pub/apps/java/gwc">download</a></p>

<h2><a class="fragid" name="history">History</a></h2>

<p>Version 0.5.1 is now packaged into uk.co.channele.gwc.  Changed title background colour to better suit the new Java (1.5.0) Swing interface.</p>

<p>Version 0.5 changes the autodetection of the log file type.  I needed to get total sizes of old messages.  The log files for message age and size are very similiar.  Internally the application handles the log file as if it were looking for large messages.</p>

<p>Version 0.4 adds autodetection of the log file type.</p>

<p>Version 0.3 was work to improve the user interface.  The program now requires a <abbr title="Graphical User Environment">GUI</abbr>.  (Previous versions would default to producing a list of proxy rights if run from a command line.)</p>

<p class="footer">copyright &copy;1994-2004 Steven J Lilley</p>


</body>
</html>