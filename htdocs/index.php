<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<?php include("includes/head.inc"); ?>
<body>

<p class="site_id">channel-e 
<span class="tagline">information in an electronic age.</span></p>

<div class="half">

<h1>What I Do</h1>
<p>If I was &quot;trading as&quot; anything it would be channel-e, but it's just me.  I aim to provide honest IT services to home users and organisations in Calderdale and surrounding districts.</p>

<h2>Programming</h2>
<p>Java, anywhere.  Desktop, <abbr title="Personal Digital Assistant">PDA</abbr>s and servers.</p>

<h2>Computers &amp; Networks</h2>
<p>PCs and servers built to order.  <a href="failure_gallery.html">Repairs</a> and upgrades.  Windows detox.  Linux.  Networks.</p>

<h2>Web Services</h2>
<p>Handcrafted, standards compliant web sites. <abbr title="Hypertext Markup Language">HTML</abbr>, <abbr title="Java Server Pages">JSP</abbr>s and servlets.  MySQL, mail, news.</p>
</div>

<div class="half">
<h1>Free Applications/Programs</h1>

<p>Released under <a href="http://www.gnu.org">GNU</a> <a href="http://www.gnu.org/licenses/gpl.html">General Public License version 3</a>.</p>

<h2>Java</h2>
<ul>
<li><a href="java_midgley.html">Midgley web site</a> (servlets)</li>
<li><a href="java_itres.html">IT Resource intranet</a> (servlets)</li>
<li><a href="java_gwc.html">GroupWise Check Parser</a> (desktop)</li>
<li><a href="java_mologo.html">Mo Logo</a> (applet)</li>
<li><a href="java_parser.html">File Parser</a> (desktop)</li>
<li><a href="java_reporter.html">Reporter &amp; Data Reader</a> (Zaurus <abbr title="Personal Digital Assistant">PDA</abbr>)</li>
<li><a href="java_www.html">Miscellaneous</a> (servlets)</li>
</ul>

<h2>Microchip PIC</h2>
<ul>
<li><a href="pic_radcover.html">Radiator Cover</a> (hardware)</li>
</ul>

<h2>Psion <abbr title="Organiser Programming Language">OPL</abbr></h2>
<ul>
<li><a href="opl_survey.html">Condition Survey</a> (Series 3)</li>
<li><a href="opl_extract.html">Data Extract</a> (Series 3)</li>
<li><a href="opl_quest.html">Questionnaire</a> (Series 3)</li>
</ul>

</div>
<?php include("includes/foot.inc"); ?> 
</body>
</html>
