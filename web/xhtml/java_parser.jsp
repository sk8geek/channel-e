<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!-- Copyright 1994-2004 Steven J Lilley -->
<head>
<title>channel-e, File Parser</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="index,follow" />
<meta name="description" content="Description of File Parser application.  An application for converting data from a Palm application to txt or csv format." />
<meta name="keywords"    content="Java, Palm, counter, parse, convert, conversion" />
<link rel="stylesheet" type="text/css" href="channel-e.css" />
<link rel="shortcut icon" href="channel-e.ico" />
</head>
<body>

<div class="menu">
<jsp:useBean id="menu" scope="page" class="uk.co.channele.www.MenuBean">
<jsp:setProperty name="menu" property="pageID" value="java_parser" />
<jsp:getProperty name="menu" property="pageMenu" />
</jsp:useBean> 
</div>

<p class="site_id">channel-e<br />
<span class="h1tag">information in an electronic age.</span></p>

<h1>(Palm) File Parser</h1>

<h2><a class="fragid" name="background">Background</a></h2>
<p>A friend had some data from a Palm handheld.  The data was from an automated counter but he couldn't read it.  This application converts it to text or comma separated value format.  (This was probably the basis for my <a href="/java_gwc.jsp">GroupWise Check log parser</a>.)</p>

<h2><a class="fragid" name="operation">Operation</a></h2>
<p>A graphical interface allows the user to select source and target files, file types and start date for the data.  The start time (usually hour zero on the start date) can be adjusted.  Zero count data can be excluded.</p>

<h2><a class="fragid" name="status">Status</a></h2>
<p>No further development planned.</p>

<p>Released under the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License</a>.  <a href="ftp://www.channel-e.co.uk/pub/apps/java/parser">download</a></p>



<p class="footer">copyright &copy;1994-2004 Steven J Lilley</p>


</body>
</html>