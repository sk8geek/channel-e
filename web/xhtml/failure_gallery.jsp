<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!-- Copyright 1994-2004 Steven J Lilley -->
<head>
<title>channel-e, Failure Gallery</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="index,follow" />
<meta name="description" content="Gallery of failed components." />
<meta name="keywords" content="failed, hardware, equipment" />
<link rel="stylesheet" type="text/css" href="channel-e.css" />
<link rel="shortcut icon" href="channel-e.ico" />
</head>
<body>

<div class="menu">
<jsp:useBean id="menu" scope="page" class="uk.co.channele.www.MenuBean">
<jsp:setProperty name="menu" property="pageID" value="pic_radcover" />
<jsp:getProperty name="menu" property="pageMenu" />
</jsp:useBean> 
</div>

<p class="site_id">channel-e<br />
<span class="h1tag">information in an electronic age.</span></p>

<h1>Failure Gallery</h1>

<p>A mixed bag of images.  Most are of component failures.</p>

<dl>
<dt>Dirty Mouse</dt>
<dd>This sent to users to show them <a href="/images/technical/mouse_dirt.jpg">mouse rollers that need cleaning</a>.  On their copy I red circled the dirt.</dd>
<dt>PSU Failure #1</dt>
<dd>ISTR that this failure caused an over voltage that destroyed the hard drive, mainboard, memory and processor in the machine.  In the PSU there was evidence of <a href="/images/technical/psu1_fail_1.jpg">resistors overheating</a> and the <a href="/images/technical/psu1_fail_2.jpg">board getting hot</a> too.</dd>
<dt>Modem Lightening Damage #1</dt>
<dd>A PCI modem.  The first image shows a <a href="/images/technical/modem1_lightening_1.jpg">surface mount resistor that has been cooked</a>.  There is evidence of damage on three of the (blue) capacitors next to the resistor.  From above the board you can see that <a href="/images/technical/modem1_lightening_2.jpg">the top of the resistor is partially missing</a>.</dd>
<dt>HP LaserJet 8100dn fuser</dt>
<dd>This failure occured during a big print run.  We think something melted inside the fuser and caused a jam.  This resulted in <a href="/images/technical/hp8100_fuser.jpg">some teeth being stripped from a gear</a>.</dd>
<dt>PSU Failure #2</dt>
<dd>This unit went with a bang.  Opening it up shows why; <a href="/images/technical/psu2_fail_1.jpg">two electrolytic capacitors had burst!</a>  The cans are shown beside the PSU.</dd>
<dt>Hot D-Link PCMCIA NIC</dt>
<dd>An <a href="/images/technical/dlink_pcmcia.jpg">illustration of just how warm</a> this card was getting!  Warm enough to bubble the label.</dd>
<dt>Fujitsu HD Failure</dt>
<dd>Fujitsu had a problem with their <a href="/images/technical/fujitsu_hd_label.jpg">MPF range</a>.  Here is the <a href="/images/technical/fujitsu_hd_bios_msg.jpg">BIOS message</a> from one of those drives.  The Fujitsu drive is primary master.</dd>
<dt>Exploding Windows CDs<dt>
<dd>Back in 2003 we had a few <a href="/images/technical/w2k_cdrom.jpg">Windows 2000 CDs shatter in the drive</a>.  On this occasion the drive survived.</dd>
<dt>Modem Lightening Damage #2</dt>
<dd>Another PCI modem.  This time it's a <a href="/images/technical/modem2_lightening_1.jpg">surface mount transistor (Q3) with it's top partially missing</a>.</dd>
</dl>

<p class="footer">copyright &copy;2001-2004 Steven J Lilley</p>

</body>
</html>