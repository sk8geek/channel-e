<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!-- Copyright 1994-2004 Steven J Lilley -->
<head>
<title>channel-e</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="index,follow" />
<meta name="description" content="channel-e" />
<meta name="keywords"    content="channel-e, information in an electronic age, comms, Psion, 3a, 3c, 3mx, software, authoring, html, web, site, website, www, design, coding, app, apps, application, applications, extract, questionnaire, people counter, APC, footpath, condition survey, program, programmer, bespoke, Java, servlet, JSP, applet" />
<link rel="stylesheet" type="text/css" href="channel-e.css" />
<link rel="shortcut icon" href="channel-e.ico" />
</head>
<body>

<p class="site_id">channel-e<br />
<span class="h1tag">information in an electronic age.</span></p>

<div class="half">

<h1>What I Do</h1>
<p>Channel-e is a small (one person) company.  I provide IT services to home users and organisations in Calderdale and surrounding districts.</p>

<h2>Programming</h2>
<p>Java, anywhere.  Desktop, <abbr title="Personal Digital Assistant">PDA</abbr>s and servers.</p>

<h2>Computers &amp; Networks</h2>
<p>PCs and servers build to order.  Repairs and upgrades.  Windows detox.  Linux.  Networks.</p>

<h2>Web Services</h2>
<p>Handcrafted, standards compliant web sites. <abbr title="Hypertext Markup Language">HTML</abbr>, <abbr title="Java Server Pages">JSP</abbr>s and servlets.  MySQL, mail, news.</p>
</div>

<div class="half">
<h1>Free Applications/Programs</h1>

<p>All under either <abbr title="General Public License">GPL</abbr> or <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache License</a>.</p>

<h2>Java</h2>
<ul>
<li><a href="java_itres.html">IT Resource</a> (servlets)</li>
<li><a href="java_gwc.jsp">GroupWise Check Parser</a> (desktop)</li>
<li><a href="java_mologo.html">Mo Logo</a> (applet)</li>
<li><a href="java_parser.html">File Parser</a> (desktop)</li>
<li><a href="java_wardens.html">Reporter &amp; Data Reader</a> (Zaurus <abbr title="Personal Digital Assistant">PDA</abbr>)</li>
</ul>

<h2>Microchip PIC</h2>
<ul>
<li><a href="pic_radrad.html">Radiator Cover</a> (hardware)</li>
</ul>

<h2>Psion <abbr title="Organiser Programming Language">OPL</abbr></h2>
<ul>
<li><a href="opl_survey.html">Footpath Condition Survey</a> (Series 3)</li>
<li><a href="opl_extract.html">Data Extract</a> (Series 3)</li>
<li><a href="opl_quest.html">Questionnaire</a> (Series 3)</li>
</ul>

<!-- 
<p><img src="images/icon_condsurv.gif" width="48" height="48" vspace="4" hspace="4" alt="[icon]" align="left" />
Enter footpath condition survey data directly on to your Psion.<br />
<a href="condition_survey.html">read more</a> | 
<a href="survey_guide.html">user guide</a></p>

<h3>Extract Application</h3>
<p><a href="extract.html"><img src="images/icon_extract.gif" width="48" height="48" vspace="4" hspace="4" alt="[icon]" align="left" border="0" /></a>
Download data from automatic people counters to your Psion.<br />
<a href="extract.html">read more</a> | 
<a href="extract_guide.html">user guide</a></p>
 -->
</div>

<!-- 
<p class="center"><a href="mailto:webmaster@channel-e.co.uk">mail me</a> | <a href="location.html">location</a></p>
 -->
 
<p class="footer">copyright &copy;1994-2004 Steven J Lilley</p>

<p class="buttons">
<a href="http://validator.w3.org/check?uri=referer">
<img style="border:0;width:88px;height:31px" src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0!" /></a>

<a href="http://jigsaw.w3.org/css-validator/validator?uri=http://www.channel-e.co.uk/channel-e.css">
<img style="border:0;width:88px;height:31px" src="images/vcss.gif" alt="Valid CSS!" /></a>

<a href="http://www.redhat.com/"><img style="border:0;width:88px;height:31px" src="images/poweredby.png" alt="Powered by RedHat" /></a>
</p>

</body>
</html>