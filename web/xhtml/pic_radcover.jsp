<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!-- Copyright 1994-2004 Steven J Lilley -->
<head>
<title>channel-e, Radiator Cover</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="index,follow" />
<meta name="description" content="Description of a radiator cover that uses a microcontroller to provide simple lighting effects." />
<meta name="keywords"    content="Microchip, PIC, radiator, cover, LED, kid, child, fun" />
<link rel="stylesheet" type="text/css" href="channel-e.css" />
<link rel="shortcut icon" href="channel-e.ico" />
</head>
<body>

<div class="menu">
<jsp:useBean id="menu" scope="page" class="uk.co.channele.www.MenuBean">
<jsp:setProperty name="menu" property="pageID" value="pic_radcover" />
<jsp:getProperty name="menu" property="pageMenu" />
</jsp:useBean> 
</div>

<p class="site_id">channel-e<br />
<span class="h1tag">information in an electronic age.</span></p>

<h1>Radiator Cover</h1>

<p>Is this the world's first hardware and software upgradable radiator cover?</p>

<h2><a class="fragid" name="background">Background</a></h2>
<p>Back in February 2004 I had to make a radiator cover for Elliot's room.  It was going to be a simple project, 6mm <abbr title="Medium Density Fibreboard">MDF</abbr> sheet with Mexican style geometric shapes cut out of it.  But as I thought about it it transformed into a week long electronics project.</p>

<h2><a class="fragid" name="construction">Construction</a></h2>
<p>The front of the radiator cover is, as planned, 6mm <abbr title="Medium Density Fibreboard">MDF</abbr> sheet glued and screwed to a frame of (something like) 40mm square timber.  This allows for various shapes to be cut from the front.  These are backed with fabric or mesh so that the heat can escape.  The illuminated shapes (all lit with 5mm <abbr title="Light Emitting Diode">LED</abbr>s) are listed below (front <a href="/images/rad_cover_front-left.jpg">left</a> and <a href="/images/rad_cover_front-right.jpg">right</a>).  There are also twenty yellow 3mm <abbr title="Light Emitting Diode">LED</abbr>s that give the effect of distant stars.  (<a href="/images/rad_cover_construction.jpg">image</a>)</p>

<ul>
<li>Four stars, 2 yellow</li>
<li>Countdown timer, 3 red</li>
<li>Rocket flame, 1 yellow and 1 orange</li>
<li><abbr title="Unidentified Flying Object">UFO</abbr>, 2 green and 2 red</li>
<li>Alien (in <abbr title="Unidentified Flying Object">UFO</abbr>), 2 green</li>
<li>Planet, 3 orange</li>
<li>Moon, 2 white</li>
</ul>

<p>I used speaker cable for wiring as it's cheapest I could find, though a little heavy for the job.  Power is provided by a 5 volt mains transformer.</p>

<p>The timing of the <abbr title="Light Emitting Diode">LED</abbr>s is controlled by a <a href="http://www.microchip.com">Microchip</a> PIC16F84 microcontroller.  There are two logic chips on the board too, a 74LS154 and a 74LS137. The <a href="radiator_cover_details.pdf">schematic diagram</a>  also gives details of the edge connections from the board to the wiring in the cover itself.  The touch sensor was never implemented.</p>

<h2><a class="fragid" name="programming">Programming</a></h2>

<p>The control program is was written in assembler and compiled used the standard Microchip MPLab application suite.  The chip was flashed with a PICStart Plus programmer.</p>

<p>The controller sleeps until INT0 on PORTB is triggered.  Note that the <abbr title="Light Emitting Diode">LED</abbr>s are strobed.  The clock speed (approx. 386KHz) was chosen to give the stars a gentle shimmering effect.  The watchdog timer is not used, though I may review this in future projects.</p>

<h2><a class="fragid" name="operation">Operation</a></h2>
<p>The sequence starts when the photo transistor detects a bright to dark transition.  The first transition causes all the stars and the planet to light.  Every few minutes the there is a countdown from three followed by the rocket launching.  Offset by a minute or so the four <abbr title="Light Emitting Diode">LED</abbr>s along the side of the <abbr title="Unidentified Flying Object">UFO</abbr> flash in sequence and the alien lights up green.  After about 12 minutes the stars extinguish.  At about 16 minutes the moon lights and a minute later the planet goes dark too.  After 20 minutes the system goes back to sleep ready for next time.</p>

<p>Some basic programming is built in.  If the bedroom lights are flashed a second time then all but one of the stars extinguish.  Another flash turns this star off too. (Another flash brings all of the stars back on.)</p>

<h2><a class="fragid" name="status">Status</a></h2>
<p>No current activity.  My next such project is a radiator cover for Imogen, with a floral theme.  For her's I want to add lighting fade effects.</p>

<p>The control program is not perfect on this.  Sometimes it crashes and stops with various bits lit up.  A reset is then required to get it going again.  For this reason I will add a rest button on the next one I do.  I would also like better sensitivity control of when it switches on.  On cloudy days it can switch on as the sun moves in and out of cloud.</p>

<p>Released under the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License</a>.  <a href="ftp://www.channel-e.co.uk/pub/apps/pic/radcover">download</a></p>

<p class="footer">copyright &copy;1994-2004 Steven J Lilley</p>

</body>
</html>