/* 
 * @(#) RadioButtonItem.java	0.1 2012/03/25
 * 
 * A radio button.
 * Copyright (C) 2003,2004,2012 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele;

import java.io.Serializable;
/**
 * A radio button.
 *
 * @version		0.1    25 March 2012
 * @author		Steven Lilley
 */
public class RadioButtonItem implements Serializable {
    
    /** Radio button id. */
    public String keyValue;
    /** Radio button value. */
    public String buttonId;
    /** Radio button label. */
    public String buttonLabel;
    
    /**
     * Constructor 
     * @param value the value used in the form. (primary key from database)
     * @param id the id to be used in HTML forms
     * @param label the human readable label for this button
     */
    public RadioButtonItem(String value, String id, String label) {
        keyValue = value;
        buttonId = id;
        buttonLabel = label;
    }
}