/* 
 * @(#) RadioButtonSet.java    0.1 2012/03/25
 * 
 * Class to provide a radio button set from a database table.
 * Copyright (C) 2003-2007,2012 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 * Class to provide a radio button control from values in a database table 
 *
 * @version       0.1   25 March 2012
 * @author        Steven Lilley
 */
abstract public class RadioButtonSet { 
    
    /** The data source for database operations. */
    protected DataSource dataSrc;
    /** A logger. */
    protected Logger log;
    /** Radio button set */
    protected ArrayList<RadioButtonItem> buttonSet;
    /** The name of the database table from which to create the radio button set. */
    protected String tableName;
    /** The name of the radio button set in the HTML form. */
    protected String buttonSetName;
    /** The title/question for the button set. */
    protected String buttonSetTitle;
    /** The id of the currently selected button in the set, or null for none. */
    protected String selectedValue;
    /** Indicator to include a text box for "Other" option. */
    protected boolean includeOther = false;
    /** Value of the "Other" text field. */
    protected String otherText;
    /** Boolean indicating that the "Other" text field is at error. */
    protected boolean otherTextError = false;
    
    public void setDataSrc(DataSource src) {
        dataSrc = src;
    }
    
    public void setLogger(Logger log) {
        this.log = log;
    }
    
    /**
     * Set the database table from which to populate the button set.
     * @param table the name of the database table holding data for this set
     * @param populateNow true if the set should be populated now
     */
    public void setTableName(String table, boolean populateNow) {
        buttonSet = new ArrayList<RadioButtonItem>();
        tableName = table;
        if (!populateNow) {
            populateButtonSet();
        }
    }
    
    /**
     * Set the button group name used in HTML forms.
     * @param name the control name to use
     */
    public void setGroupName(String name) {
        buttonSetName = name;
    }
    
    public void setGroupTitle(String title) {
        buttonSetTitle = title;
    }
    
    public void setNameAndTitle(String name, String title) {
        setGroupName(name);
        setGroupTitle(title);
    }
    
    /**
     * Indicate is the radio button set should include an "other" text field
     * @param incOther set true is an "other" text box should be included
     */
    public void setIncludeOther(boolean incOther) {
        includeOther = incOther;
    }
    
    /**
     * Set the value of the "other" text field.
     * @param otherValue the value of the "other" field
     */
    public void setOtherText(String otherValue) {
        otherText = otherValue;        
    }
    
    /**
     * Set the error state of the "other" text field
     * @param error set true if the field is at error
     */
    public void setOtherTextError(boolean error) {
        otherTextError = error;
    }
    
    /**
     * Set the id of the currently selected button
     * @param id the id of the currently selected button
     */
    public void setSelectedValue(String value) {
        selectedValue = value;
    }
    
    /**
     * Returns an HTML text input form control.
     * @param selectedValue the value of the selected button, null if none selected
     * @param reqd true if the field is mandatory
     * @return the HTML text input field
     */
    public String getRadioButtons(String selectedValue, boolean reqd, boolean error) {
        StringBuilder h = new StringBuilder();
        ListIterator<RadioButtonItem> iterator = buttonSet.listIterator();
        RadioButtonItem item;
        String itemId = "";
        if (error) {
            h.append("<div class=\"control-group error\">");
        } else {
            h.append("<div class=\"control-group\">");
        }
        h.append("<label class=\"radio-set\">");
        h.append(buttonSetTitle);
        if (reqd) {
            h.append("<span class=\"required\">*</span>");
        }
        h.append("</label>");
        h.append("<div class=\"radio\">");
        while (iterator.hasNext()) {
            item = iterator.next();
            h.append("<input type=\"radio\" name=\"" + buttonSetName + "\" ");
            h.append("value=\"" + item.keyValue + "\" id=\"" + item.buttonId + "\"");
            if (selectedValue != null) {
                if (selectedValue.length() > 0) {
                    if (item.keyValue.equalsIgnoreCase(selectedValue)) {
                        h.append(" checked=\"true\"");
                    }
                }
            }
            h.append(">");
            h.append("<label for=\"" + item.buttonId + "\">");
            h.append(item.buttonLabel + "</label>\n");
            itemId = item.buttonId;
        }
        h.append("</div>");
        if (includeOther && itemId.length() > 0) {
            if (otherTextError) {
                h.append("<div class=\"control-group error\">");
            } else {
                h.append("<div class=\"control-group\">");
            }
            h.append("<input name=\"" + buttonSetName + "Other\" id=\"");
            h.append(itemId + "Text\" size=\"40\" type=\"text\">");
            h.append("</div>");
        }
        h.append("</div>");
        return h.toString();
    }

    /**
     * Get an HTML <code>option</code> string using the values submitted.
     * @param value the value returned by the form
     * @param text the humabe readable value presented in the option
     * @param selected true is this option is currently selected
     */
    static private String addOption(String value, String text, boolean selected) {
        StringBuilder h = new StringBuilder();
        h.append("<option value=\"");
        h.append(value);
        h.append("\"");
        if (selected) {
            h.append(" checked=\"true\"");
        }
        h.append(">");
        h.append(text);
        h.append("</option>\n");
        return h.toString();
    }
    
    /**
     * Populates the drop down list from the database table
     */
    abstract protected void populateButtonSet();
}

