/* 
 * @(#) MenuBean.java    0.3 2007/03/17
 * 
 * A Bean to provide a menu for web pages.
 * Copyright (C) 2006-2007 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@sk8hx.org.uk
 */
package uk.co.channele;

import java.io.Serializable;
/**
 * Provides a tabbed menu.
 * <p>The arrays holding the page and options names should be overriden by extending classes. Page names must be full 
 * names, including <code>.jsp</code> is necessary. It is recommended that no more than six menu options are included 
 * if the target audience might be using a display size of 800 x 600 pixels. If an Admin option is included that the 
 * <code>hasAdminTab</code> should be set. Display of this tab is controlled by the <code>enableAdminTab</code> flag.
 * <p>0.4 Added lots of comments. Added sub-menu functionality.
 * <p>0.3 Wow, abstract stuff is cool!
 * <p>0.2 Uses StringBuilder rather than StringBuilder (requires Java 1.5!)
 *
 * @version       0.4    17 March 2007
 * @author        Steven Lilley
 */
abstract public class MenuBean implements Serializable { 
    
    /** An array of the page names that the options represent. */
    protected String[] pageNames;
    /** An array of the options presented to the user. */
    protected String[] optionNames;
    /** The currently selected page. */
    protected String currentPage = "";
    /** The parent page of a submenu. */
    protected String parentPage;
    /** Flag to indicate that an Admin option is included. */
    protected boolean hasAdminTab = false;
    /** Flag to indicate if an <code>Admin</code> should be displayed. */
    protected boolean enableAdminTab = false;
    /** Flag to indicate if this is a second level (submenu) bean. */
    protected boolean subMenuBean = false;
    
    /**
     * Set the bean as a sub-menu bean.
     * @param submenu true if the menu should behave as a sub-menu
     */
    public void setSubMenuBean(boolean submenu) {
        subMenuBean = submenu;
    }
    
    /**
     * Set the parent page. This is only used if this bean is a subMenuBean.
     * @param parent the parent page
     */
    public void setParentPage(String parent) {
        parentPage = parent;
    }
    
    /**
     * Sets the current page name, used to determine how the menu tabs are displayed.
     * File type extensions are removed from the page name.
     * @param page current page name
     */
    public void setPageName(String page) {
        if (page.indexOf(".") > -1) {
            currentPage = page.substring(0, page.indexOf("."));
        } else {
            currentPage = page;
        }
    }
    
    /**
     * Sets the current page name, used to determine how the menu tabs are displayed.
     * File type extensions are removed from the page name.
     * @param page current page name
     */
    public void setPage(String page) {
        setPageName(page);
    }
    
    /**
     * Get menu tabs as HTML
     * @return an HTML snippet containing the menu tabs for this page
     */
    public String getMenuTabs() {
        StringBuilder h = new StringBuilder();
        int i = 0;
        int lastItemToShow = optionNames.length;
        if (hasAdminTab && !enableAdminTab) {
            lastItemToShow--;
        }
        if (subMenuBean) {
            h.append("<p class=\"subMenu\">");
        } else {
            h.append("<p class=\"menu\">");
        }
        for (i = 0; i < lastItemToShow; i++) {
            h.append(drawMenuTab(i));
        }
        h.append("</p>");
        return h.toString();
    }
    
    /**
     * Set the page names to the given values.
     * @param pages an array of the page names
     */
    protected void setPageNames(String[] pages) {
        pageNames = pages;
    }
    
    /**
     * Set the option names to the given values.
     * @param options an array of the displayed option names
     */
    protected void setOptionNames(String[] options) {
        optionNames = options;
    }
    
    /**
     * Set the Admin option flag to the given value.
     * @param enable true to enable the Admin option
     */
    public void setAdminTab(boolean enable) {
        enableAdminTab = enable;
    }
    
    /**
     * Get the menu tab for the given tab in the array.
     * @param tabNumber the menu tab to draw.
     * @return an HTML snippet containing the menu tab
     */
    private String drawMenuTab(int tabNumber) {
        StringBuilder h = new StringBuilder();
        String page = "";
        String option = pageNames[tabNumber];
        h.append("<a class=\"");
        if (option.indexOf(".") > -1) {
            page = option.substring(0, option.indexOf("."));
        } else {
            page = option;
        }
        if (subMenuBean) {
            if (page.equalsIgnoreCase(currentPage)) {
                h.append("subSelected\" ");
            } else {
                h.append("subMenuTab\" ");
            }
        } else {
            if (page.equalsIgnoreCase(currentPage)) {
                h.append("selected\" ");
            } else {
                h.append("menuTab\" ");
            }
        }
        h.append("href=\"");
        if (subMenuBean) {
            h.append(parentPage + "?page=" + option);
        } else {
            h.append(option);
        }
        h.append("\">");
        h.append(optionNames[tabNumber]);
        h.append("</a>");
        if (tabNumber < (optionNames.length - 1)) {
            if (subMenuBean) {
                h.append("<span class=\"subSpacer\"> | </span>\n");
            } else {
                h.append("<span class=\"spacer\"> | </span>\n");
            }
        }
        return h.toString();
    }
}

