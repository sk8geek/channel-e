/* 
 * @(#) DropDownBean.java    0.5 2007/07/21
 * 
 * Bean to provide Drop Down (HTML SELECT control) function.
 * Copyright (C) 2003-2007 Steven J Lilley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@sk8hx.org.uk
 */
package uk.co.channele;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
/**
 * Class providing core Drop Down methods.
 * <p>0.5 Improved handling of null data set.
 *
 * @version       0.5    21 July 2007
 * @author        Steven Lilley
 */
abstract public class DropDownBean implements Serializable { 
    
    /** A date format used by MySQL. */
    static final SimpleDateFormat mysqlDate = new SimpleDateFormat("yyyy-MM-dd");
    /** A date/time used by MySQL. */
    static final SimpleDateFormat mysqlDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    /** A long day name format. */
    static final SimpleDateFormat itemDay = new SimpleDateFormat("EEEE");
    /** A date format, giving the month as a three character string. */
    static final DateFormat itemDate = DateFormat.getDateInstance(DateFormat.MEDIUM);
    /** A time format, omitting seconds. */
    static final DateFormat itemTime = DateFormat.getTimeInstance(DateFormat.SHORT);
    /** The starting content for naming operations. */
    private InitialContext initCntx;
    /** The naming context. */
    private Context cntx;
    /** The data source for database operations. */
    private DataSource dataSrc;
    /** A logger. */
    private Logger log;
    /** The drop down list */
    private ArrayList<DropDownItem> dropDownList;
    /** The name of the database table from which to create the drop down list. */
    private String tableName;
    /** The name of the control to use in HTML forms using this drop down list. */
    private String controlName = "";
    /** The default value to pre-select in the drop down list. */
    private String homeValue = "";
    /** The currently selected value in the drop down list. */
    private String selectedValue = "";
    /** Flag to indicate if multiple values can be selected from the list. */
    private boolean allowMultipleChoice = false;
    /** Flag to indicate is a null value should be included in the list. */
    private boolean allowNullValue = false;
    /** Flag to indicate that the control in an HTML form should include a submit on change function. */
    private boolean autoSubmit = false;
    /** Flag to indicate that the JavaScript <code>dropDownChanged</code> function should be called on change. */
    private boolean notifyOnChange = false;
    /** Flag to indicate that the human readable values should be used as <code>values</code>s in HTML forms. */
    private boolean returnHumanValues = false;
    /** Flag to indicate that the list has been populated with values. */
    private boolean listPopulated = false;
    
    /**
     * Set the database table (or view) name from which to populate the drop down list. If the list isn't already 
     * populated then population from the table takes place. The flag indicating the the list has been populated is set.
     * @param table the name of the database table holding data for this list
     */
    public void setTableName(String table) {
        tableName = table;
        if (!listPopulated) {
            populateDropDownList();
            listPopulated = true;
        }
    }
    
    /**
     * Set the control name used in HTML forms.
     * @param name the control name to use
     */
    public void setControlName(String name) {
        controlName = name;
    }
    
    /**
     * Set the default value for the list. This also sets the currently selected value to the default value.
     * @param home the default value in the list
     */
    public void setHomeValue(String home) {
        homeValue = home;
        selectedValue = home;
    }
    
    /**
     * Set the currently selected value in the list.
     * @param currentValue the currently selected value
     */
    public void setSelectedValue(String currentValue) {
        selectedValue = currentValue;
    }
    
    /**
     * Set the flag indicating that human values should be used for <code>values</code>s in HTML forms.
     * @param forHumans true is human readable values should be used
     */
    public void setHumanValues(boolean forHumans) {
        returnHumanValues = forHumans;
    }
    
    /**
     * Set the selected value back to the default value.
     */
    public void setGoHome() {
        selectedValue = homeValue;
    }
    
    /**
     * Set the flag indicating that a null value should be included in the list.
     * @param allowNull set to <code>true</code> (case insensitive) to enable a null value, all other values reset the flag
     */
    public void setAllowNullValue(String allowNull) {
        if (allowNull.equalsIgnoreCase("true")) {
            allowNullValue = true;
        } else {
            allowNullValue = false;
        }
    }
    
    /**
     * Set the flag indicating that a null value should be included in the list.
     * @param allowNull true to enable a null value, false to disable the null value
     */
    public void allowNullValue(boolean allowNull) {
        allowNullValue = allowNull;
    }
    
    /**
     * Set the flag indicating that the JavaScript function <code>dropDownChanged</code> function should be called on change. 
     * If the flag is set then the automatic submit on change flag is reset.
     * @param notify true to set the flag, false to reset it.
     */
    public void setNotifyOnChange(boolean notify) {
        notifyOnChange = notify;
        if (notifyOnChange) {
            autoSubmit = false;
        }
    }
    
    /**
     * Get an HTML <code>select</code> control for this drop down list.
     * <p>If the list hasn't been populated then an empty string is returned.
     * @return the HMTL control
     */
    public String getSelectControl() {
        StringBuilder h = new StringBuilder();
        int i = 0;
        if (dropDownList.size() > 0) {
            h.append("<select name=\"");
            h.append(controlName);
            h.append("\"");
            if (autoSubmit) {
                h.append(" onChange=\"submit();\"");
            } 
            if (notifyOnChange) {
                h.append(" onChange=\"dropDownChanged('" + controlName + "', this.value);\"");
            }
            h.append(">");
            if (allowNullValue) {
                h.append("<option value=\"\"");
                if (selectedValue.length() == 0 || selectedValue == null) {
                    h.append(" selected");
                }
                h.append(">");
            }
            for (i = 0; i < dropDownList.size(); i++) {
                DropDownItem listValue = (DropDownItem)dropDownList.get(i);
                h.append("<option value=\"");
                if (returnHumanValues) {
                    h.append(listValue.desc);
                } else {
                    h.append(listValue.code);
                }
                h.append("\"");
                if (selectedValue.length() > 0) {
                    if (listValue.code.equalsIgnoreCase(selectedValue) ) {
                        h.append(" selected");
                    }
                }
                h.append(">" + listValue.desc + "</option>\n");
            }
            h.append("</select>");
        }
        return h.toString();
    }
    
    /**
     * Get an HTML <code>select</code> control giving a rough expiry date. This control includes values for today, 
     * tomorrow, days for the next week and dates up to twenty one days from today.
     * @return the HTML control
     */
    static public String getRoughExpirySelectControl() {
        StringBuilder h = new StringBuilder();
        Calendar cal = Calendar.getInstance();
        int dayCounter = 0;
        h.append("<select name=\"expiry\">");
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        // today
        h.append(addOption(mysqlDateTime.format(cal.getTime()), "Today", false));
        // tomorrow
        cal.add(Calendar.DATE, 1);
        h.append(addOption(mysqlDateTime.format(cal.getTime()), "Tomorrow", false));
        // a few more weeks
        do {
            cal.add(Calendar.DATE, 1);
            dayCounter++;
            if (dayCounter < 7) {
                h.append(addOption(mysqlDateTime.format(cal.getTime()), itemDay.format(cal.getTime()), false));
            } else {
                h.append(addOption(mysqlDateTime.format(cal.getTime()), itemDate.format(cal.getTime()), false));
            }
        } while (dayCounter < 22);
        h.append("</select>");
        return h.toString();
    }
    
    /**
     * Get an HTML <code>option</code> string using the values submitted.
     * @param value the value returned by the form
     * @param text the humabe readable value presented in the option
     * @param selected true is this option is currently selected
     */
    static private String addOption(String value, String text, boolean selected) {
        StringBuilder h = new StringBuilder();
        h.append("<option value=\"");
        h.append(value);
        h.append("\"");
        if (selected) {
            h.append(" selected");
        }
        h.append(">");
        h.append(text);
        h.append("</option>\n");
        return h.toString();
    }
    
    /**
     * Populates the drop down list from the database table (view)
     */
    abstract protected void populateDropDownList();
}

